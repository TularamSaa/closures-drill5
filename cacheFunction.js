function cacheFunction(cb) {
  let cache = {};
  let invokeFunction = function (x) {
    if (cache.hasOwnProperty(x) === false) {
      cache[x] = 1;
      cb(x);
    } else {
      cache[x] += 1;
      console.log(x);
    }
    // console.log(cache);
  };
  return invokeFunction;
}

module.exports = cacheFunction;
