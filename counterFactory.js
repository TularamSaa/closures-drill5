const counterFactory = (() => {
  count = 1;
  console.log(`innitial value ${count}`);
  let counter = {
    increment() {
      return `${(count += 1)} , value is increasing`;
    },
    decrement() {
      return `${(count -= 1)} , value is decreasing`;
    },
  };

  return counter;
})();

module.exports = counterFactory;
