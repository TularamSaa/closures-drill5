function checklimitFunctionCallCount(cb, n) {
  let count = 0;
  function resultFunct() {
    if (count < n) {
      count++;
      return cb();
    } else {
      return null;
    }
  }
  return resultFunct;
}

module.exports = checklimitFunctionCallCount;
