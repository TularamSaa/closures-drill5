let getData = require("../limitFunctionCallCount");

let testModule = getData(function () {
  console.log("cb function is executing...,\n");
}, 4);

testModule();
testModule();

testModule();
testModule();
testModule();
testModule();
